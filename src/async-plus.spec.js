const chai = require("chai");
const { expect } = chai;
const asyncPlus = require("./async-plus");
chai.use(require("chai-as-promised"));

describe("async-plus", function() {
  describe("regular async function", () => {
    it("should work as a regular async function", () => {
      let exampleFunction = asyncPlus(function*(a, b) {
        let result = (yield Promise.resolve(a)) + (yield Promise.resolve(b));
        return result;
      });
      let result = exampleFunction(1, 2);
      return expect(result).to.eventually.eql(3);
    });

    it("should handle promise rejections", () => {
      let exampleFunction = asyncPlus(function*(a, b) {
        try {
          yield Promise.reject("some error");
        } catch (ex) {
          return yield {
            result: (yield Promise.resolve(a)) + (yield Promise.resolve(b)),
            ex
          };
        }
      });

      let result = exampleFunction(1, 2).then(x => x.result);
      return expect(result).to.eventually.eql(3);
    });
  });

  describe("aync plus object support", () => {
    it("support yielding array of promises", () => {
      let exampleFunction = asyncPlus(function*(a, b) {
        let result = yield [Promise.resolve(a), Promise.resolve(b)];
        return result[0] + result[1];
      });
      let result = exampleFunction(1, 2);
      return expect(result).to.eventually.eql(3);
    });

    it("support yielding object of promises", () => {
      let exampleFunction = asyncPlus(function*(a, b) {
        let result = yield { a: Promise.resolve(a), b: Promise.resolve(b) };
        return result.a * result.b;
      });
      let result = exampleFunction(1, 2);
      return expect(result).to.eventually.eql(2);
    });

    it("support yielding of nested complex object of promises", () => {
      let exampleFunction = asyncPlus(function*(a, b) {
        let result = yield {
          d: {
            e: [Promise.resolve(a), { q: Promise.resolve(b) }]
          }
        };
        return result.d.e[0] - result.d.e[1].q;
      });
      let result = exampleFunction(1, 2);
      return expect(result).to.eventually.eql(-1);
    });
  });
});
