### Async plus exercise

Implement an "async plus" function that takes a generator function and simulate async-await behavior with some upgrades 
that can make parallel processing easier.

The api should support:

1. 

```javascript
asyncPlus(function*(){
	const o = yield {a: getAData(), b:getBData()} 
	return o.a + o.b; 
})
```
instead of: 
```javascript
async function(){
	const aPromise = getAData();
	const bPromise = getBData(); 
	const [a,b] = await Promise.all(aPromise,bPromise);
	return a +b; // or can be return (await aPromise) + (await bPromise);
}
```

2. 

```javascript
asyncPlus(function*(){
   const [a,b] = yield [getAData(), b:getBData()]
   return a + b;
})
 ```

instead of: 
```javascript
async function(){
	const [a,b] = await Promise.all([getAData(), b:getBData()])
	return a + b;
} 
```

- This behavior should also applied to nested objects (see spec, async-plus.spec.js).
- Try implementing it without the usage of async-await.
- It should follow regular async-await capbilites such as error handling and support non-promise primtives. (see spec)


### How to run tests

Install dependencies with yarn/npm.

Use:
```yarn test``` or ```jest```
